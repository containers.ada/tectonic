FROM docker.io/rust:latest as builder
RUN apt-get update && apt-get install -y libfontconfig1-dev libgraphite2-dev libharfbuzz-dev libicu-dev zlib1g-dev
RUN cargo install --locked tectonic

FROM docker.io/debian:stable-slim
RUN apt-get update \
     && apt-get install -y --no-install-recommends curl unzip fontconfig libfontconfig1 libgraphite2-3 libharfbuzz0b libicu72 zlib1g libharfbuzz-icu0 ca-certificates \
    && rm -rf /var/lib/apt/lists/* 

WORKDIR /tmp
RUN set -eux; \
    curl -sSLO https://github.com/googlefonts/atkinson-hyperlegible/archive/main.zip; \
    echo "e4fad2ddc1874e7b9a42edb8be7f63366ee060b2527fecdcf4ab73eab57b48cb  main.zip" | sha256sum -c; \
    unzip main.zip; \
    install -m 644 atkinson-hyperlegible-main/fonts/ttf/*.ttf /usr/local/share/fonts; \
    fc-cache -v
    
COPY --from=builder /usr/local/cargo/bin/tectonic /usr/bin/
WORKDIR /usr/src/tex
